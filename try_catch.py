import sys

for arg in sys.argv[1:]:
  try:
    f = open(arg,'r')
  except IOError:
    print('cant open',arg)
  else:
    print(arg,'has',len(f.readlines()),'lines')
    f.close()
